 
# 微信支付V2官方处于维护模式，所以本项目也处于维护模式，如果对V3有兴趣的请移步 [payment-spring-boot](https://notfound403.github.io/payment-spring-boot)
 
## **WePay-支付接口JAVA封装**
 
 
#### **目标**
 - 不需要过分关注支付本身的支付逻辑 只关注支付过程中具体业务的实现
 
 
#### **微信支付UML**
 ![UML](https://git.oschina.net/uploads/images/2017/0816/151415_9f389a6d_975445.png "UML.png") 
 

 
 
#### **wiki**
 - 具体使用配置方法参看腾讯文档或者 wiki：[https://gitee.com/felord/WePay/wikis](https://gitee.com/felord/WePay/wikis)
 - demo项目：[https://gitee.com/felord/WePayDemo](https://gitee.com/felord/WePayDemo)
 
#### **maven坐标**
 
 ```   
 <dependency>
   <groupId>cn.felord</groupId>
   <artifactId>wePay</artifactId>
   <version>1.0.4</version>
 </dependency>
 ```  



#### **仓库地址**
- wepay-spring-boot-starter: [https://gitee.com/felord/wepay-spring-boot](https://gitee.com/felord/wepay-spring-boot)
 - OSC:[https://git.oschina.net/felord/WePay](https://git.oschina.net/felord/WePay)
 - GitHub:[https://github.com/NotFound403/WePay](https://github.com/NotFound403/WePay)
 
 
 
 